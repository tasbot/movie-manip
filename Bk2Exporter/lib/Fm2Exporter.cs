namespace Net.TASBot.Bk2Exporter {
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;

	using BizHawk.Client.Common;
	using BizHawk.Common;
	using BizHawk.Emulation.Common;

	/// <remarks>https://fceux.com/web/help/fm2.html</remarks>
	[ExporterFor("fm2")]
	public sealed class Fm2Exporter : IMovieExporter {
		[RequiredApi] // from CLI, never checked; from ext. tool, needs to fail iff unmet
		public IEmuClientApi? EmuClientApi { get; set; }

		public bool Export(IMovie movie, Stream outputStream, out IReadOnlyList<string> errors, out IReadOnlyList<string> warnings) {
			List<string> warningList = new();
			warnings = warningList;
#pragma warning disable CA2000 // see finally block
			StreamWriter outputSW = new(outputStream);
#pragma warning restore CA2000
			try {
				if (movie.SystemID is not VSystemID.Raw.NES) throw new Exception("cannot serialise movie as .fm2 when system is not NES");
				outputSW.WriteLine("version 3");
				outputSW.WriteLine("emuVersion 22020"); // apparently this is for 2.2.3; TASVideos.org doesn't use this field, and BizHawk doesn't care
				outputSW.WriteLine($"rerecordCount {movie.Rerecords}");
#pragma warning disable CS0162
				if (/*movie.SyncSettingsJson.*/false) outputSW.WriteLine("palFlag 1"); //TODO parse sync settings
				if (false) outputSW.WriteLine("NewPPU 1"); //TODO no idea
				if (movie.BoardName is "FDS") outputSW.WriteLine("FDS 1");
				if (false) { //TODO other peripherals
					outputSW.WriteLine("fourscore 1");
					outputSW.WriteLine("port2 0");
				} else {
					outputSW.WriteLine("fourscore 0");
					outputSW.WriteLine("port0 1");
					outputSW.WriteLine("port1 1");
					outputSW.WriteLine("port2 0");
				}
#pragma warning restore CS0162
				var totalFrames = movie.InputLogLength;
				outputSW.WriteLine($"length {totalFrames}");
				var md5Checksum = new byte[16];
				var romFilename = EmuClientApi is null ? null : EmuClientApi.HackGetCurrentlyOpenRom();
				if (string.IsNullOrEmpty(romFilename)) {
					warningList.Add("rom filename+checksum could not be determined automatically and must be added manually (or, rerun in EmuHawk with a rom loaded)");
					romFilename = "unknown";
				} else {
					md5Checksum = MD5Checksum.Compute(File.ReadAllBytes(romFilename));
					romFilename = Path.GetFileNameWithoutExtension(romFilename);
				}
				outputSW.WriteLine($"romFilename {romFilename}");
				outputSW.WriteLine($"comment author {movie.Author}");
				outputSW.WriteLine("guid 00000000-0000-0000-0000-000000000000"); // was going to gen a random one, but then the tests wouldn't work
				outputSW.WriteLine($"romChecksum base64:{Convert.ToBase64String(md5Checksum)}");
				static void ProcessGamepad(StreamWriter sw, in ReadOnlySpan<char> mnemonicSubStr) {
					sw.Write(mnemonicSubStr[3] is 'R' ? 'R' : '.');
					sw.Write(mnemonicSubStr[2] is 'L' ? 'L' : '.');
					sw.Write(mnemonicSubStr[1] is 'D' ? 'D' : '.');
					sw.Write(mnemonicSubStr[0] is 'U' ? 'U' : '.');
					sw.Write(mnemonicSubStr[4] is 'S' ? 'T' : '.');
					sw.Write(mnemonicSubStr[5] is 's' ? 'S' : '.');
					sw.Write(mnemonicSubStr[6] is 'B' ? 'B' : '.');
					sw.Write(mnemonicSubStr[7] is 'A' ? 'A' : '.');
				}
				for (var frame = 0; frame < totalFrames; frame++) {
					var frameMnemonic = movie.GetInputLogEntry(frame);
					const byte commands = 0; //TODO "commands" (reset, etc.)
					outputSW.Write($"|{commands}|");
					ProcessGamepad(outputSW, frameMnemonic.AsSpan(start: 4, length: 8));
					outputSW.Write('|');
					if (frameMnemonic.Length < 14) outputSW.Write("........");
					else ProcessGamepad(outputSW, frameMnemonic.AsSpan(start: 13, length: 8));
					outputSW.WriteLine("||");
				}
				errors = new List<string>();
				return true;
#pragma warning disable CA1031
			} catch (Exception e) {
#pragma warning restore CA1031
				errors = new List<string> { $"{e.Message}\n{e.StackTrace}" };
				return false;
#if NET6_0
			} finally {
				outputSW.Close(); // under net48, this closes the wrapped stream, causing the file write to fail
#endif
			}
		}
	}
}
