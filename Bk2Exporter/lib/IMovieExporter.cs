namespace Net.TASBot.Bk2Exporter {
	using System.Collections.Generic;
	using System.IO;

	using BizHawk.Client.Common;

	public interface IMovieExporter {
		bool Export(IMovie movie, Stream outputStream, out IReadOnlyList<string> errors, out IReadOnlyList<string> warnings);
	}
}
