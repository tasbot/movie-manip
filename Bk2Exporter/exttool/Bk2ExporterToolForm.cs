namespace Net.TASBot.Bk2Exporter {
	using System;
	using System.Drawing;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Windows.Forms;

	using BizHawk.Client.Common;
	using BizHawk.Client.EmuHawk;
	using BizHawk.Common.PathExtensions;
	using BizHawk.Emulation.Common;
	using BizHawk.WinForms.Controls;

	[ExternalTool("Bk2Exporter")]
	public sealed class Bk2ExporterToolForm : ToolFormBase, IExternalToolForm {
		private bool FirstRestart = true;

		private IMovie? LoadedMovie;

		private readonly TextBox MovieFilenameTextbox;

		protected override string WindowTitleStatic { get; } = "Bk2Exporter";

		public Bk2ExporterToolForm() {
			ClientSize = new(320, 64);
			SuspendLayout();
			MovieFilenameTextbox = new SzTextBoxEx { Size = new(100, 23) };
			MovieFilenameTextbox.TextChanged += (_, _) => LoadedMovie = null;
			var comboFileExt = new ComboBox { Size = new(64, 23) };
			comboFileExt.Items.AddRange(Utils.ExporterForFileExt.Keys.Cast<object>().ToArray());
			var btnConvert = new SzButtonEx { Size = new(40, 23), Text = "=>" };
			var lblMessageReadout = new SzLabelEx { Dock = DockStyle.Fill };
			btnConvert.Click += (_, _) => {
				try {
					lblMessageReadout.Text = DoConversion(
						movieFilename: MovieFilenameTextbox.Text,
						LoadedMovie,
						targetFileExt: (string) comboFileExt.SelectedItem,
						((MainForm) MainForm).Config!,
						(IExternalApiProvider) typeof(ToolManager).GetField("_apiProvider", BindingFlags.Instance | BindingFlags.NonPublic)!.GetValue(Tools)
					);
#pragma warning disable CA1031
				} catch (Exception e) {
#pragma warning restore CA1031
					lblMessageReadout.Text = e.Message;
					new ExceptionBox(e).ShowDialog();
				}
			};
			Controls.Add(new LocSzSingleColumnFLP {
				Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
				Controls = {
					new SingleRowFLP {
						Controls = {
							new LabelEx { Text = "Convert" },
							MovieFilenameTextbox,
							new LabelEx { Text = "to" },
							comboFileExt,
							btnConvert,
						},
						Dock = DockStyle.Fill,
					},
					lblMessageReadout,
				},
				Location = new(4, 4),
				Size = new(ClientSize.Width - 8, ClientSize.Height - 8),
			});
			ResumeLayout();
		}

#if false
		public override bool AskSaveChanges() => true; // this (virtual impl. from ToolFormBase) is incorrect but I imagine nobody will be dumb enough to close the window during conversion
#endif

		/// <remarks>I initially put some complicated "has user changed textbox" stuff in here to update the textbox when changing movies, but it didn't work</remarks>
		public override void Restart() {
			var loadedMovie = ((MainForm) MainForm).MovieSession.Movie; // sorry adelikat
			if (!FirstRestart) {
				// leave the textbox untouched iff it was unmodified before the restart, and the restart wasn't due to a change in loaded movie
#pragma warning disable CS8602 // call to Filename getter after IsActive has returned true
				if (LoadedMovie != null && (!loadedMovie.IsActive() || loadedMovie.Filename != MovieFilenameTextbox.Text)) MovieFilenameTextbox.Text = string.Empty; // TextChanged handler unsets LoadedMovie
#pragma warning restore CS8602
				return;
			}
			// init
			FirstRestart = false;
			if (!loadedMovie.IsActive()) return;
#pragma warning disable CS8602 // same as above
			var movieFilename = loadedMovie.Filename;
#pragma warning restore CS8602
			MovieFilenameTextbox.Text = movieFilename.Substring(movieFilename.LastIndexOf(Path.DirectorySeparatorChar) + 1);
			LoadedMovie = loadedMovie;
		}

		private static string DoConversion(
			string movieFilename,
			IMovie? loadedMovie,
			string targetFileExt,
			Config config,
			IExternalApiProvider apiProvider
		) {
			if (!Utils.ExporterForFileExt.TryGetValue(targetFileExt, out var exporterType)) return $"internal error: could not find exporter for .{targetFileExt}";
			if (!(Activator.CreateInstance(exporterType) is IMovieExporter exporter)) return $"internal error: failed to instantiate .bk2 to .{targetFileExt} exporter";
			if (!ApiInjector.UpdateApis(apiProvider, exporter)) return $"internal error: some services required by .{targetFileExt} exporter could not be satisfied";
#pragma warning disable CA2000
			var outputStream = new MemoryStream();
#pragma warning restore CA2000
			var targetFilename = $"{movieFilename.Substring(0, movieFilename.Length - 4)}.{targetFileExt}";
			if (loadedMovie != null) targetFilename = Path.Combine(GetGlobalMoviePath(config), targetFilename);
			if (!exporter.Export(loadedMovie ?? Utils.LoadMovieFromFile(movieFilename), outputStream, out var errors, out var warnings)) {
				Console.WriteLine($"{errors.Count} errors and {warnings.Count} warnings from conversion to {targetFilename}:\n{string.Join("\n", errors)}\n\n{string.Join("\n", warnings)}");
				return $"converter failed with {errors.Count} errors, the first is {errors[0]}";
			}
			try {
				using var targetFile = File.Create(targetFilename);
				outputStream.WriteTo(targetFile);
			} catch (IOException e) {
				return $"failed to write converted movie to file: {e.Message}";
			}
			if (warnings.Count == 0) return $"success, wrote {targetFilename}";
			Console.WriteLine($"{warnings.Count} warnings from conversion to {targetFilename}:\n{string.Join("\n", warnings)}");
			return $"success, wrote {targetFilename} with {warnings.Count} warnings, the first is: {warnings[0]}";
		}

		private static string GetGlobalMoviePath(Config config) {
			var fromConfig = config.PathEntries.MovieAbsolutePath();
			return fromConfig.StartsWith("%exe%", StringComparison.InvariantCulture)
				? Path.Combine(PathUtils.ExeDirectoryPath, fromConfig.Substring(5))
				: fromConfig.StartsWith($".{Path.DirectorySeparatorChar}", StringComparison.InvariantCulture)
					? Path.Combine(PathUtils.ExeDirectoryPath, fromConfig.Substring(2))
					: PathUtils.ExeDirectoryPath;
		}
	}
}
