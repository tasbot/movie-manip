#!/bin/sh
cd "$(dirname "$0")/exttool"
if [ -z "$BIZHAWK_HOME" ]; then export BIZHAWK_HOME="$PWD/../../BizHawk/submodule/output"; fi
dotnet build -c Release -m "$@"
