using System;

using SharpCompress.Archives;
using SharpCompress.Archives.Zip;
using SharpCompress.Common;

public static class Program {
	public static void Main(string[] args) {
		var cwd = Environment.CurrentDirectory;
		var opts = new ExtractionOptions() { ExtractFullPath = true, Overwrite = true };
		using var archive = ZipArchive.Open(args[0]);
		foreach (var entry in archive.Entries) if (!entry.IsDirectory) entry.WriteToDirectory(cwd, opts);
	}
}
