namespace Net.TASBot.Bk2Exporter {
	using System;
	using System.IO;

	public static class Program {
		public static int Main(string[] args) {
			static void Log(string s) => Console.WriteLine(s);
			static void LogIndent(string s) => Console.WriteLine($"\t{s.Replace("\n", "\n\t", StringComparison.InvariantCulture)}");
			int RunWithFDs() {
				Console.WriteLine(args[0]);
				return 0;
			}
			int RunWithFilenames() {
				// sanitise inputs
				var movieFilename = args[0];
				if (!movieFilename.EndsWith(".bk2", StringComparison.InvariantCultureIgnoreCase)) throw new Exception("input movie must have the file extension .bk2");
				string targetFilename;
				string targetFileExt;
				var secondArgPeriodIndex = args[1].LastIndexOf('.');
				if (secondArgPeriodIndex == -1) {
					targetFileExt = args[1];
					targetFilename = $"{movieFilename.Substring(0, movieFilename.Length - 4)}.{targetFileExt}";
				} else {
					targetFilename = args[1];
					targetFileExt = targetFilename.Substring(secondArgPeriodIndex + 1);
				}
				if (!Utils.ExporterForFileExt.TryGetValue(targetFileExt, out var exporterType)) {
					Log($"this tool cannot export bk2 to {targetFileExt}");
					return 0;
				}

				// process
				if (!(Activator.CreateInstance(exporterType) is IMovieExporter exporter)) throw new Exception("failed to instantiate exporter");
				using var targetFile = File.Create(targetFilename) ?? throw new Exception("failed to open target file for writing");
				if (!exporter.Export(Utils.LoadMovieFromFile(movieFilename), targetFile, out var errors, out var warnings)) {
					Log($"conversion to {targetFileExt} failed with {errors.Count} {(errors.Count == 1 ? "error" : "errors")}");
					foreach (var error in errors) LogIndent(error);
					if (warnings.Count != 0) Log($"there were also {warnings.Count} {(warnings.Count == 1 ? "warning" : "warnings")}");
					foreach (var warning in warnings) LogIndent(warning);
					return 1;
				}
				Log($"conversion to {targetFileExt} succeeded with {warnings.Count} {(warnings.Count == 1 ? "warning" : "warnings")}");
				foreach (var warning in warnings) LogIndent(warning);
				return 0;
			}
			try {
				switch (args.Length) {
					case 1:
						return RunWithFDs();
					case 2:
						return RunWithFilenames();
					default:
						Log("usage:\n\twith filenames: `./bk2export from.bk2 to.lsmv` or `./bk2export from.bk2 lsmv`\n\t(NOT IMPLEMENTED) with redirection (mutes errors): `./bk2export lsmv <from.bk2 >to.lsmv` or `gen-bk2-movie | ./bk2export lsmv >to.lsmv`");
						return 0;
				}
#pragma warning disable CA1031
			} catch (Exception e) {
#pragma warning restore CA1031
				Log($"fatal error: {e.Message}\n{e.StackTrace}");
				return 1;
			}
		}
	}
}
