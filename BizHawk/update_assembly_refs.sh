#!/bin/sh
cd "$(dirname "$0")"
mkdir -p "assemblies"
cd "assemblies"
rm -f *
for f in "EmuHawk.exe" "dll/BizHawk.Client.Common.dll" "dll/BizHawk.Common.dll" "dll/BizHawk.Emulation.Common.dll" "dll/BizHawk.Emulation.Cores.dll" "dll/BizHawk.Emulation.DiscSystem.dll" "dll/BizHawk.WinForms.Controls.dll"; do
	cp "../submodule/output/$f" .
done
